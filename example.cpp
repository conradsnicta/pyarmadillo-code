// g++ -Og -Wall -shared -std=c++11 -fPIC `python3 -m pybind11 --includes` example.cpp -o example`python3-config --extension-suffix`

#include <pybind11/pybind11.h>
#include <pybind11/complex.h>

namespace py = pybind11;

int
add_a(int i, int j)
  {
  return i + j;
  }


int
add_b(int i, int j)
  {
  return i + j;
  }


struct
Pet
  {
  Pet(const std::string &in_name) : name(in_name) {}
  
  void set_name(const std::string &in_name) { name = in_name; }
  
  const std::string& get_name() const { return name; }
  
  std::string name;
  };



struct Dog : public Pet
  {
  Dog(const std::string &in_name) : Pet(in_name) {}
  
  std::string bark() const { return "woof!"; }
  };



PYBIND11_MODULE(example, m)
  {
  m.doc() = "pybind11 example plugin"; // optional module docstring
  
  // NOTE: within Python, stuff declared via m.def() is by default read/write, including changing the type
  m.def("add_a", &add_a, "A function which adds two numbers", py::arg("i"), py::arg("j"));
  
  m.def("add_b", &add_b, "A function which adds two numbers", py::arg("i") = 1, py::arg("j") = 2);
  
  // NOTE: within Python, stuff declared via m.attr() is by default read/write, including changing the type
  m.attr("the_answer") = 42;
  
  // explicit conversion via py::cast()
  // https://pybind11.readthedocs.io/en/stable/advanced/cast/index.html
  py::object world = py::cast("World");
  
  m.attr("what") = world;  
  
  py::class_<Pet>(m, "Pet")
    .def(py::init<const std::string&>())
    .def("set_name", &Pet::set_name)
    .def("get_name", &Pet::get_name)
    .def("__repr__", [](const Pet &a) { return "<example.Pet named '" + a.name + "'>"; }  )
  //.def_readwrite("name", &Pet::name)  // for read/write fields
    .def_readonly("name", &Pet::name)  // for const fields
    ;
  
  // Static member functions can be bound in the same way using class_::def_static().
  
  py::class_<Dog, Pet>(m, "Dog")
      .def(py::init<const std::string&>())
      .def("bark", &Dog::bark);
  
  }
