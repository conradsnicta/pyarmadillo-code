// g++ -Og -Wall -shared -std=c++11 -fPIC `python3 -m pybind11 --includes` example.cpp -o example`python3-config --extension-suffix`

// import pyarma
// to reload module in python3:
// import importlib
// importlib.reload(pyarma)


// python3 operators:
// https://docs.python.org/3/library/operator.html

#include <string>
#include <sstream>
#include <initializer_list>

#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <pybind11/complex.h>
#include <pybind11/iostream.h>

#include <armadillo>

namespace py = pybind11;


using namespace arma;


// TODO: what to do with exceptions?

PYBIND11_MODULE(pyarma, m)
  {
  m.doc() = ( std::string("PyArmadillo: Python Interface to Armadillo") + std::string("\nPyArmadillo version: [TODO]\nArmadillo version: ") + arma_version::as_string() );
  
  py::class_<arma::mat>(m, "mat")
    .def_readonly("n_rows", &mat::n_rows)
    .def_readonly("n_cols", &mat::n_cols)
    .def_readonly("n_elem", &mat::n_elem)
    .def("__repr__", [](const mat &a)
      {
      std::stringstream ss;
      ss << "<pyarma.mat; " << "size: " << a.n_rows << 'x' << a.n_cols << "; addr: " << &a << '>';
      return ss.str();
      })
    .def(py::init())
    .def(py::init<const std::string&>())
    .def(py::init([](const uword n_rows, const uword n_cols)
      {
      return std::unique_ptr<mat>(new mat(n_rows, n_cols, arma::fill::zeros));
      }), py::call_guard<py::scoped_estream_redirect>())
    //
    .def(py::init([](const std::vector<double>& list)
      {
      mat* mat_ptr = new mat;
      mat& mat_ref = *mat_ptr;
      mat_ref.set_size(1,list.size());
      std::memcpy(mat_ref.memptr(), &list[0], list.size() * sizeof(double));
      return std::unique_ptr<mat>(mat_ptr);
      }), py::call_guard<py::scoped_estream_redirect>())
    //
    .def(py::init([](const std::vector< std::vector<double> >& outer_list)
      {
      mat* mat_ptr = new mat;
      mat& mat_ref = *mat_ptr;
      
      uword list_n_rows = outer_list.size();
      uword list_n_cols = 0;
      
      for(uword i=0; i < list_n_rows; ++i)  { list_n_cols = (std::max)(uword(list_n_cols), uword(outer_list[i].size())); }
      
      mat_ref.zeros(list_n_rows, list_n_cols);
      
      for(uword i=0; i < list_n_rows; ++i)
        {
        const std::vector<double>& inner_list = outer_list[i];        
        const uword inner_list_size = uword(inner_list.size());
        
        for(uword j=0; j < inner_list.size(); ++j)  { mat_ref(i,j) = inner_list[j]; }
        }
      return std::unique_ptr<mat>(mat_ptr);
      }), py::call_guard<py::scoped_estream_redirect>())
    //.def() // TODO: initialiser for numpy data
    .def("zeros", [](mat& A) { A.zeros(); } )
    .def("zeros", [](mat& A, const uword n_elem) { A.zeros(n_elem); }, py::call_guard<py::scoped_estream_redirect>() )
    .def("zeros", [](mat& A, const uword n_rows, const uword n_cols) { A.zeros(n_rows, n_cols); }, py::call_guard<py::scoped_estream_redirect>() )
    .def("randu", [](mat& A) { A.randu(); } )
    .def("randu", [](mat& A, const uword n_elem) { A.randu(n_elem); }, py::call_guard<py::scoped_estream_redirect>() )
    .def("randu", [](mat& A, const uword n_rows, const uword n_cols) { A.randu(n_rows, n_cols); }, py::call_guard<py::scoped_estream_redirect>() )
    .def("print", [](const mat& A) { A.print(); }, py::call_guard<py::scoped_ostream_redirect, py::scoped_estream_redirect>() )
    .def("print", [](const mat& A, const std::string& header) { A.print(header); }, py::call_guard<py::scoped_ostream_redirect, py::scoped_estream_redirect>() )
    .def("__getitem__", [](const mat& A, const uword index) { return A(index); }, py::call_guard<py::scoped_estream_redirect>() )
    .def("__getitem__", [](const mat& A, const std::pair<uword,uword>& indices) { return A(indices.first,indices.second); }, py::call_guard<py::scoped_estream_redirect>() )
    .def("__setitem__", [](      mat& A, const uword index, const double val) { A(index) = val; }, py::call_guard<py::scoped_estream_redirect>() )
    .def("__setitem__", [](      mat& A, const std::pair<uword,uword>& indices, const double val) { return A(indices.first,indices.second) = val; }, py::call_guard<py::scoped_estream_redirect>() )
    // TODO: handle subviews
    ;
  
  py::class_<arma::vec, arma::mat>(m, "vec")
    .def(py::init<>())
    //.def(py::init<const std::string&>())
    //.def("bark", &Dog::bark)
    ;
  }
