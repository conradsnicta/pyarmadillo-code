#!/usr/bin/env python3

import pyarma

def main():
  
  X = pyarma.mat([1,2,3])
  print(X)
  X.print("X:");
  
  X = pyarma.mat([ [1,2,3], [4,5,6] ])
  print(X)
  X.print("X:");
  
  X = pyarma.mat(4,5)
  print(X)
  X.print("X:");
  
  X.randu();
  print(X)
  X.print("X:");
  
  X[1,2] = 123;
  X.print("X:");
  
  # X[4,5] = 123;  # should cause an exception
  # X.print("X:");
  


if __name__ == "__main__":
  main()
